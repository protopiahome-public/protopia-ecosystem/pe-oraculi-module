import React, {Suspense} from "react"
import { Query } from "react-apollo"
import BasicState from "react-pe-basic-view"
import { __ } from "react-pe-utilities"
import {Loading} from 'react-pe-useful'
import { getQueryArgs, getQueryName, queryCollection } from "react-pe-layouts"

import CourseCard from "./tutor/CourseCard"

class TutorCourses extends BasicState {
	getRoute = () => "all-courses"

	addRender() {
		const query_name = getQueryName("Bio_Course")
		const query_args = getQueryArgs("Bio_Course")
		const query = queryCollection("Bio_Course", query_name, query_args)

		return (
			<div className="course-card-list">
				<Query query={query}>
					{
						({
							loading, error, data, client,
						}) => {
							if (loading) {
								return <Loading />
							}
							if (data) {
								const current_course_id = this.props.user
									? this.props.user.current_course ? this.props.user.current_course.id : -1
									: -1
								const current_course_title = this.props.user
									? this.props.user.current_course ? this.props.user.current_course.post_title : ""
									: ""

								const courses = data[query_name].filter((e) => e.parent == 0 || !e.parent)
								console.log(courses)
								const coursesList = courses.map((e, i) => <CourseCard user={this.props.user} {...e} key={i} i={i} />)
								return [coursesList]
							}
						}
					}
				</Query>
			</div>
		)
	}
}
export default (TutorCourses)
