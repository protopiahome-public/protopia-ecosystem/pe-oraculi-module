import React, { Component } from "react" 
import { withRouter } from "react-router"
import { compose } from "recompose" 
import { __ } from "react-pe-utilities"  
import TestQuote from "./TestQuote"
//import Feed from "../../../../layouts/BasicState/Feed"
import { Feed } from "react-pe-basic-view"
import { withApollo } from "react-apollo"

class CourseTestList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      numberposts: 10,
      offset: 0,
    }
  }

  render() {
    const { id, articles } = this.props
    const data_type = "Bio_Test"
    const paging = `taxonomies :{ tax_name : "${this.props.type}", term_ids : [ ${id} ] }, order:"ASC"`

    return (
      <Feed
        component={TestQuote}
        data_type={data_type}
        offset={0}
        paging={paging}
      />
    )
  }
}

export default compose(
  withApollo,
  withRouter,
)(CourseTestList)
