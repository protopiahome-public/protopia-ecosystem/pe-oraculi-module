import React, { Component } from "react" 
import { withRouter } from "react-router"
import { compose } from "recompose" 
import { __ } from "react-pe-utilities"  
import { initArea } from "react-pe-utilities" 

class CurrentCourse extends Component {
  constructor(props) {
    super(props)
    this.state = {

    }
  }

  render() {
    const { course, user } = this.props
    return (
      <div className="container">
        <div>
          {
				initArea(
				  "curent-course-content",
				  { ...this.props },
				)
			}
        </div>
      </div>
    )
  }
}
export default compose(
  withRouter,
)(CurrentCourse)
