import React, { Component, Suspense } from "react"
import { NavLink } from "react-router-dom" 
import { __ } from "react-pe-utilities" 
import {Loading} from 'react-pe-useful' 
const OwlCarousel = React.lazy(() => import( 'react-owl-carousel2fix' ))

class TutorFacultetResultsWidget extends Component {
  constructor(props) {
    super(props)
    this.car = React.createRef()
    this.po = React.createRef()
  }

  render() {
    if (this.props.bio_image_result.length == 0) return ""
    const options = {
      loop: true,
      autoplay: true,
      margin: 0,
      items: 1,
      responsive: {
        0: {
          items: 1,
        },
        1000: {
          items: 1,
        },
        1200: {
          items: 1,
        },
      },
    }
    const bio_image_result = this.props.bio_image_result.map((e, i) => (
      <NavLink
        className="w-100"
        key={i}
        to={`/result/${e.id}`}
      >
        <div className="">
          {e.post_title}
        </div>
        <div
          style={{
						  backgroundImage: `url(${e.thumbnail})`,
						  width: "100%",
						  height: 300,
						  backgroundSize: "cover",
						  backgroundPosition: "center",
          }}
        />
      </NavLink>
    ))
    const coursesList = (<Suspense fallback={<Loading/>}>
      <OwlCarousel ref={this.car} options={options}>
        {bio_image_result}
      </OwlCarousel>
    </Suspense>)
    return (
      <div>
        <div className="aside-widget-title">
          {__("Our graduates results:")}
        </div>
        {coursesList}
      </div>
    )
  }
}
export default TutorFacultetResultsWidget
