import React, { Component } from "react"
import { compose } from "recompose"
import { withApollo } from "react-apollo"
import gql from "graphql-tag"
import { Intent, Button, ButtonGroup } from "@blueprintjs/core"
import { __ } from "react-pe-utilities" 

class EventUserUser extends Component {
  render() {
    const {
      display_name, user_email, id, event_id,
    } = this.props
    return (
      <div className="row mx-1">
        <div className="col-md-4 py-1 border-bottom border-secondary">
          {display_name}
        </div>
        <div className="col-md-4 py-1 border-bottom border-secondary">
          {user_email}
        </div>
        <div className="col-md-4 py-1 border-bottom border-secondary">
          <ButtonGroup>
            <Button intent={Intent.SUCCESS} onClick={this.onAccept}>
              {__("Agree")}
            </Button>
            <Button intent={Intent.DANGER} onClick={this.onRefuse}>
              {__("Refuse")}
            </Button>
          </ButtonGroup>
        </div>
      </div>
    )
  }

  onAccept = () => {
    /*
    if(this.props.onAccept)
      this.props.onAccept();
    return;
    */

    const user_id = this.props.id
    const { event_id } = this.props
    const mutation = gql`
			mutation Accept_Request
			{
			  Accept_Request(input:{
				user:${user_id},
				event: ${event_id}
			  })
			}
		`
    this.props.client.mutate({ mutation })
      .then((result) => {
        console.log(result)
        if (this.props.onAccept) this.props.onAccept()
      })
  }

  onRefuse = () => {
    if (this.props.onRefuse) this.props.onRefuse()
  }
}
export default compose(
  withApollo,
)(EventUserUser)
