import React, { Component, } from "react"
import { compose } from "recompose"
import { withApollo } from "react-apollo"
import gql from "graphql-tag"
import { __ } from "react-pe-utilities"

class QuestionHiddenLabelWidget extends Component {
	state = {
		col: 0,
	}

	componentDidMount() {
		if (this.props.data.data_type == "Bio_TestQuestion") {
			const query = gql`
				query getFullQuestionHiddenCount
				{
					getFullQuestionHiddenCount
				}
			`
			this.props.client.query({ query })
				.then((result) => {
					// console.log( result );
					this.setState({ col: result.data.getFullQuestionHiddenCount })
				})
		}
	}

	render() {
		// console.log( this.props.data );
		return this.props.data.data_type == "Bio_TestQuestion"
			? (
				<>
					{
						this.state.col > 0
							? (
								<div className="menu-labla hint hint--left" data-hint={__("Need approve")}>
									{this.state.col}
								</div>
							)
							: null
					}
				</>
			)
			: null
	}
}
export default compose(
	withApollo,
)(QuestionHiddenLabelWidget)
