function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import $ from "jquery";
import { __ } from "react-pe-utilities";
import LessonMainComment from "./Comments/LessonMainComment";

class LessonBlockComments extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      is_answer: false,
      answeHeight: 0,
      comment: ""
    });

    _defineProperty(this, "onAnswer", () => {
      this.setState({
        is_answer: !this.state.is_answer,
        answeHeight: !this.state.is_answer ? $("#comment_0 .answer textarea").height() + 7 : 0
      });
    });

    _defineProperty(this, "onComment", evt => {
      const {
        value
      } = evt.currentTarget;
      this.setState({
        comment: value
      });
    });
  }

  render() {
    const {
      lesson,
      comments
    } = this.props; // const _comments = comments
    // 	.filter(e => e.comment_id == 0)
    // 	.map((e, i) =>
    // 	{
    // 		return <LessonMainComment
    // 			key={i}
    // 			{...e}
    // 			lesson={lesson}
    // 			level={0}
    // 		/>
    // 	});

    const main_comment = comments.filter(e => e.comment_id === 0)[0];

    const _comments = /*#__PURE__*/React.createElement(LessonMainComment, _extends({
      key: 0
    }, main_comment, {
      lesson: lesson,
      level: 0
    }));

    return /*#__PURE__*/React.createElement("div", {
      className: "px-3 pt-3 borded-top"
    }, /*#__PURE__*/React.createElement("div", {
      className: "title pl-0"
    }, "\u041A\u043E\u043C\u043C\u0435\u043D\u0442\u0430\u0440\u0438\u0438"), /*#__PURE__*/React.createElement("div", {
      className: "comment",
      id: "comment_0"
    }, /*#__PURE__*/React.createElement("div", {
      className: "answer d-flex",
      style: {
        height: this.state.answeHeight
      }
    }, /*#__PURE__*/React.createElement("textarea", {
      value: this.state.comment,
      onChange: this.onComment,
      rows: "4"
    }), /*#__PURE__*/React.createElement("div", {
      className: "ml-3 btn btn-primary"
    }, __("Ответить"))), /*#__PURE__*/React.createElement("div", {
      className: "ml-auto btn btn-primary",
      onClick: this.onAnswer
    }, __(this.state.is_answer ? "Закрыть" : "Новый комментарий"))), _comments, /*#__PURE__*/React.createElement("span", {
      className: "title-collapsed showed pointer btn-link"
    }, "\u041F\u043E\u043A\u0430\u0437\u0430\u0442\u044C \u0435\u0449\u0435"));
  }

}

export default LessonBlockComments;