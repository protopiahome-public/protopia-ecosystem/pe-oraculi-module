import React, { Component } from "react";
import { withRouter } from "react-router";
import { compose } from "recompose";
import { __ } from "react-pe-utilities";
import LessonQuote from "./LessonQuote"; //import Feed from "../../../../layouts/BasicState/Feed"

import { Feed } from "react-pe-basic-view";
import { withApollo } from "react-apollo";

class CourseArticleList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      numberposts: 10,
      offset: 0
    };
  }

  render() {
    const {
      id,
      articles
    } = this.props;
    const data_type = "Bio_Article";
    const paging = `taxonomies :{ tax_name : "${this.props.type}", term_ids : [ ${id} ] }, order_by_meta:"order", order:"ASC"`;
    return /*#__PURE__*/React.createElement(Feed, {
      component: LessonQuote,
      data_type: data_type,
      offset: 0,
      paging: paging
    });
  }

}

export default compose(withApollo, withRouter)(CourseArticleList);