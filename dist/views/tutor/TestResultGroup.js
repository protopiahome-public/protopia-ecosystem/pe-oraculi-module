function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import Moment from "react-moment";
import { NavLink } from "react-router-dom";
import { Button } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";

class TestResultGroup extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onOpen", () => this.setState({
      isOpen: !this.state.isOpen
    }));

    this.state = {
      isOpen: false,
      id: this.props.tests[0].test.id
    };
  }

  componentDidUpdate(nextProps) {// if(nextProps.openId)
    //	this.setState({isOpen: nextProps.openId !== this.state.id});
  }

  render() {
    const dup = [];
    let i = 0;
    let n = 0;

    for (const ee in this.props.tests) {
      // console.log(ee);
      dup.push( /*#__PURE__*/React.createElement("div", {
        className: "row test-result-sub",
        key: i
      }, /*#__PURE__*/React.createElement("div", {
        className: "col-md-6"
      }, "--"), /*#__PURE__*/React.createElement("div", {
        className: "col-md-1"
      }, this.props.tests[ee].right_count), /*#__PURE__*/React.createElement("div", {
        className: "col-md-1"
      }, this.props.tests[ee].credits), /*#__PURE__*/React.createElement("div", {
        className: "col-md-3"
      }, this.props.tests[ee].end_time && this.props.tests[ee].start_time ? /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
        className: "mb-1"
      }, /*#__PURE__*/React.createElement(Moment, {
        locale: "ru",
        format: "D.MM.YYYY h:mm:ss"
      }, new Date(this.props.tests[ee].start_time * 1000))), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Moment, {
        locale: "ru",
        format: "D.MM.YYYY h:mm:ss"
      }, new Date(this.props.tests[ee].end_time * 1000)))) : "--")));
      n = ee;
      i++;
    }

    dup.pop();
    const e = this.props.tests[n];
    const bio_test_category = e.bio_test_category.map((ee, ii) => /*#__PURE__*/React.createElement("div", {
      className: "tutor-label"
    }, ee.post_title));
    console.log(e);
    return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
      className: "row test-result"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-md-6"
    }, /*#__PURE__*/React.createElement(NavLink, {
      to: `/test/${e.test.id}`,
      className: ""
    }, /*#__PURE__*/React.createElement("div", {
      dangerouslySetInnerHTML: {
        __html: e.test.post_title
      }
    })), /*#__PURE__*/React.createElement("div", null, bio_test_category)), /*#__PURE__*/React.createElement("div", {
      className: "col-md-1"
    }, e.right_count), /*#__PURE__*/React.createElement("div", {
      className: "col-md-1"
    }, e.credits), /*#__PURE__*/React.createElement("div", {
      className: "col-md-3"
    }, e.start_time && e.end_time ? /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
      className: "mb-1"
    }, /*#__PURE__*/React.createElement(Moment, {
      locale: "ru",
      format: "D.MM.YYYY h:mm:ss"
    }, new Date(e.start_time * 1000))), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Moment, {
      locale: "ru",
      format: "D.MM.YYYY h:mm:ss"
    }, new Date(e.end_time * 1000)))) : "--"), /*#__PURE__*/React.createElement("div", {
      className: "col-md-1 pr-0",
      onClick: this.onOpen
    }, /*#__PURE__*/React.createElement(Button, {
      rightIcon: this.state.isOpen ? "caret-down" : "caret-right",
      text: dup.length + 1,
      fill: true
    })), /*#__PURE__*/React.createElement("div", {
      className: `col-12 ${this.state.isOpen ? "" : "hidden"}`
    }, dup)));
  }

}

export default TestResultGroup;