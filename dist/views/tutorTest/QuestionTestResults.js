import React, { Component } from "react";
import { Icon, Intent } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";

class QuestionTestResults extends Component {
  render() {
    console.log(this.props);
    const {
      result,
      origin
    } = this.props;

    if (!result) {
      return null;
    }

    const is_right = result && result.answers.filter(e => result.right.filter(ee => ee == e).length > 0).length > 0;
    const rights = result.right.map((e, i) => {
      const text = origin.answers.filter(ee => ee.id == e); // console.log(text)

      return /*#__PURE__*/React.createElement("div", {
        key: i
      }, /*#__PURE__*/React.createElement("div", {
        className: "small text-success"
      }, /*#__PURE__*/React.createElement("span", null, __("right answer"), " ", ":"), /*#__PURE__*/React.createElement("span", {
        className: "text-bold"
      }, text[0] ? text[0].post_content : "---")));
    });
    const your = !is_right ? result.answers.map((e, i) => {
      const text = origin.answers.filter(ee => ee.id == e); // console.log(text)

      return /*#__PURE__*/React.createElement("div", {
        key: i
      }, /*#__PURE__*/React.createElement("div", {
        className: "small text-danger"
      }, /*#__PURE__*/React.createElement("span", null, __("Your answer"), " ", ":"), /*#__PURE__*/React.createElement("span", {
        className: "text-bold"
      }, text[0] ? text[0].post_content : "---")));
    }) : null;
    const sign = is_right ? /*#__PURE__*/React.createElement(Icon, {
      icon: "tick",
      intent: Intent.SUCCESS
    }) : /*#__PURE__*/React.createElement(Icon, {
      icon: "cross",
      intent: Intent.DANGER
    });
    return /*#__PURE__*/React.createElement("div", {
      className: "my-2 row dat2"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-md-11 p-3"
    }, /*#__PURE__*/React.createElement("div", null, origin.post_title), rights, your), /*#__PURE__*/React.createElement("div", {
      className: "col-md-1 p-3"
    }, sign));
  }

}

export default QuestionTestResults;