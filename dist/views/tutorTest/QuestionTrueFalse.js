function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Intent, Button } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";

class QuestionTrueFalse extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onStep", () => {
      this.props.onStep();
    });

    _defineProperty(this, "onYes", () => {
      this.setState({
        checked: "1"
      });
      this.onChange("1");
    });

    _defineProperty(this, "onNo", () => {
      this.setState({
        checked: "0"
      });
      this.onChange("0");
    });

    _defineProperty(this, "onChange", data => {
      // console.log(data);
      this.props.onChange(data);
    });

    this.state = {
      checked: -2,
      step: this.props.step,
      start: this.props.start,
      rights: this.props.rights || []
    };
    this.timeout = -1;
  }

  componentWillUpdate(nextProps) {
    if (nextProps.checked != null && nextProps.checked !== this.state.checked) {
      console.log(nextProps.nextProps, this.state.nextProps);
      this.setState({
        checked: nextProps.checked
      });
    }

    if (nextProps.start !== this.state.start) {
      console.log(nextProps.start, this.state.start);
      this.setState({
        start: nextProps.start
      });
    }

    if (nextProps.rights && nextProps.rights !== this.state.rights) {
      console.log(nextProps.rights);
      this.setState({
        rights: nextProps.rights
      });
    }
  }

  render() {
    console.log(this.state.checked);
    const thumbnail = this.props.thumbnail ? /*#__PURE__*/React.createElement("img", {
      src: this.props.thumbnail,
      alt: this.props.post_title,
      className: "test-thumbnail"
    }) : null;
    const className = this.state.start ? " animated animation-swipe-right " : " hidden";
    return /*#__PURE__*/React.createElement("div", {
      className: `test-cover ${className}`
    }, /*#__PURE__*/React.createElement("h1", null, "TRUEFLSE"), /*#__PURE__*/React.createElement("div", {
      className: "test-question-title"
    }, this.props.post_title), thumbnail, /*#__PURE__*/React.createElement("div", {
      className: "test-question-answers-cont"
    }, /*#__PURE__*/React.createElement(Button, {
      intent: this.state.checked === "1" ? Intent.SUCCESS : "",
      onClick: this.onYes,
      large: true,
      className: "px-sm-5 mr-2",
      icon: this.state.checked === "1" ? "tick" : "blank",
      rightIcon: "blank"
    }, __("Yes")), /*#__PURE__*/React.createElement(Button, {
      intent: this.state.checked === "0" ? Intent.DANGER : "",
      onClick: this.onNo,
      large: true,
      className: "px-sm-5",
      icon: this.state.checked === "0" ? "tick" : "blank",
      rightIcon: "blank"
    }, __("No"))), /*#__PURE__*/React.createElement("div", {
      className: "mt-4"
    }, /*#__PURE__*/React.createElement(Button, {
      onClick: this.onStep
    }, __("No"))));
  }

}

export default QuestionTrueFalse;