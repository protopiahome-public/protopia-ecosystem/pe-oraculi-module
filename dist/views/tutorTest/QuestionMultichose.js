function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Button } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import AnswerRadio from "./AnswerRadio";
import AnswerCheckbox from "./AnswerCheckbox";

class QuestionMultichose extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onStep", () => {
      this.props.onStep();
    });

    _defineProperty(this, "onChange", data => {
      console.log(data);
      this.props.onChange(data);
    });

    this.state = {
      checked: [],
      step: this.props.step,
      start: this.props.start,
      rights: this.props.rights || []
    };
    this.timeout = -1;
  }

  componentWillUpdate(nextProps) {
    // console.log( nextProps.rights );
    if (nextProps.checked !== this.state.checked) {
      this.setState({
        checked: nextProps.checked
      });
    }

    if (nextProps.start !== this.state.start) {
      this.setState({
        start: nextProps.start
      });
    }

    if (nextProps.rights && nextProps.rights !== this.state.rights) {
      // console.log( nextProps.rights )
      this.setState({
        rights: nextProps.rights
      });
    }
  }

  render() {
    // console.log(this.props.single, this.state.checked);
    const answers = this.props.answers.map((e, i) => {
      const is_right = this.state.rights.filter(ee => ee.id === e.id).length > 0;
      console.log(this.state.checked ? this.state.checked.toString().split(",").filter(ee => ee === e.id).length > 0 : false);
      console.log(this.state.checked ? this.state.checked.toString().split(",") : "", e.id);
      return this.props.single ? /*#__PURE__*/React.createElement(AnswerRadio, _extends({}, e, {
        key: i,
        id: e.id,
        value: e.id,
        name: this.props.id,
        checked: this.state.checked === e.id,
        onChange: this.onChange,
        step: this.props.step,
        is_right: is_right
      })) : /*#__PURE__*/React.createElement(AnswerCheckbox, _extends({}, e, {
        key: i,
        id: e.id,
        value: e.id,
        name: this.props.id,
        checked: this.state.checked ? this.state.checked.toString().split(",").filter(ee => ee === e.id).length > 0 : false,
        onChange: this.onChange,
        step: this.props.step,
        is_right: is_right
      }));
    });
    const thumbnail = this.props.thumbnail ? /*#__PURE__*/React.createElement("img", {
      src: this.props.thumbnail,
      alt: this.props.post_title,
      className: "test-thumbnail"
    }) : null;
    const className = this.state.start ? " animated animation-swipe-right " : " hidden";
    return /*#__PURE__*/React.createElement("div", {
      className: `test-cover ${className}`
    }, /*#__PURE__*/React.createElement("div", {
      className: "test-question-title"
    }, this.props.post_title), /*#__PURE__*/React.createElement("h1", null, this.props.single ? "SINGLE" : "MULTI"), thumbnail, /*#__PURE__*/React.createElement("div", {
      className: "test-question-answers-cont"
    }, answers), /*#__PURE__*/React.createElement("div", {
      className: "mt-4"
    }, /*#__PURE__*/React.createElement(Button, {
      onClick: this.onStep
    }, __("Next"))));
  }

}

export default QuestionMultichose;