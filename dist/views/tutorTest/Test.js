function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { compose } from "recompose";
import { withApollo } from "react-apollo";
import gql from "graphql-tag";
import { __ } from "react-pe-utilities";
import { Loading } from 'react-pe-useful';
import Cover from "./Cover";
import TimerSlider from "./TimerSlider";
import ProgressSlider from "./ProgressSlider";
import Step from "./Step";
import Fail from "./Fail";
import Finish from "./Finish";

class Test extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onStart", () => {
      this.setState({
        progress: 1,
        is_timed: true,
        start_time: Date.now(),
        isTitleOpen: true
      });
    });

    _defineProperty(this, "step", (dt, id) => {
      this.preFinish();

      if (this.props.is_show_answer) {
        const question_id = `"${id}"`;
        const mutation = gql`
				mutation getRightAnswer
				{
				  getRightAnswer(question_id:${question_id})
				  {
					id
					question_id
					post_content 
					fraction
					position
				  }
				  
				}
			`;
        this.props.client.mutate({
          mutation,
          update: (store, {
            data
          }) => {
            console.log(data.getRightAnswer);
            this.setState({
              rights: data.getRightAnswer
            });
            const mem = setTimeout(() => {
              const results = [...this.state.results];
              results.push(dt);
              this.setState({
                progress: this.state.progress + 1,
                results
              });
            }, 2000);
          }
        });
      } else {
        const results = [...this.state.results];
        results.push(dt);
        this.setState({
          progress: this.state.progress + 1,
          results,
          checked: -2
        });
        console.log(results);
      }
    });

    _defineProperty(this, "onFinish", (desission, time) => {
      console.log(desission, time);
      this.setState({
        is_timed: false,
        desission
      }, () => this.preFinish());
    });

    _defineProperty(this, "onRestart", () => {
      this.props.client.query({
        query: this.props.query,
        variables: this.props.variables
      }).then(result => {
        console.log(result.data.getBio_Test);
        this.setState({ ...result.data.getBio_Test,
          len: result.data.getBio_Test.questions.length,
          progress: 0,
          full_results: undefined,
          is_timed: false,
          isTitleOpen: false,
          start_time: 0,
          end_time: 0,
          results: [],
          desission: null
        });
      });
    });

    console.log(this.props);
    this.state = { ...props,
      progress: 0,
      len: this.props.questions.length,
      is_timed: false,
      isTitleOpen: false,
      start_time: 0,
      end_time: 0,
      results: [],
      rights: []
    };
  }

  componentWillUnmount() {}

  render() {
    return /*#__PURE__*/React.createElement("div", {
      className: "test-container"
    }, /*#__PURE__*/React.createElement("div", {
      className: `test-title-collapse${this.state.isTitleOpen ? " active" : ""}`
    }, /*#__PURE__*/React.createElement("span", {
      className: "ml-3 mr-3"
    }, __("Test"), ":"), /*#__PURE__*/React.createElement("span", {
      className: "",
      dangerouslySetInnerHTML: {
        __html: this.state.post_title.replace(/<[^>]*>?/gm, "")
      }
    })), /*#__PURE__*/React.createElement(TimerSlider, {
      is_timed: this.state.is_timed,
      full: this.state.duration,
      onFinish: this.onFinish,
      progress: this.state.progress
    }), /*#__PURE__*/React.createElement(ProgressSlider, {
      progress: this.state.progress,
      full: this.state.len
    }), this.doProgress());
  }

  doProgress() {
    // console.log(this.state.progress, this.state.len);
    if (this.state.desission == "fail") {
      return /*#__PURE__*/React.createElement(Fail, null);
    }

    if (this.state.progress == 0) {
      return /*#__PURE__*/React.createElement(Cover, _extends({}, this.state, {
        step: this.onStart
      }));
    }

    if (this.state.progress == this.state.len) {
      // console.log(this.state);
      return !this.state.full_results ? /*#__PURE__*/React.createElement(Loading, null) : /*#__PURE__*/React.createElement(Finish, {
        id: this.state.id,
        try_count: this.state.try_count,
        post_title: this.state.post_title,
        questions: this.state.questions,
        onRestart: this.onRestart,
        full_results: this.state.full_results
      });
    }

    return /*#__PURE__*/React.createElement(Step, _extends({}, this.state.questions[this.state.progress - 1], {
      step: this.step,
      progress: this.state.progress,
      i: this.state.progress - 1,
      rights: this.state.rights
    }));
  }

  preFinish() {
    if (this.state.progress == this.state.len - 2 || this.state.desission == "fail") {
      const params = {};
      params.test = `"${this.state.id}"`;
      params.questions = `[ ${this.state.results.join(", ")} ]`;
      params.start_time = parseInt(this.state.start_time / 1000);
      params.end_time = parseInt(Date.now() / 1000); // console.log(params);

      let paramsStr = "";

      for (const i in params) {
        paramsStr += ` ${i}: ${params[i]} `;
      }
      /*
      console.log("mutation compareTest  { compareTest(result : { " + paramsStr + " }) { id user_id user_login user_display_name test{ id } right_count start_time end_time credits bio_test_category { id }  questions  { id  answers right }}}");
      */


      const mutation = gql`
				mutation compareTest 
				{
					compareTest(result : {${paramsStr}})
					{
						id
						test 
						{
							id
							post_title
						}
						right_count 
						start_time 
						end_time 
						credits 
						bio_test_category 
						{
							id
							post_title
						}						
						questions
						{
						  id
						  answers
						  right
						}
					}
				}
			`;
      this.props.client.mutate({
        mutation,
        update: (store, {
          data
        }) => {
          this.setState({
            full_results: data.compareTest,
            progress: this.state.len - 1,
            is_timed: false
          });
        }
      });
    }
  }

}

export default compose(withApollo)(Test);