import React, { Component } from "react";
import { compose } from "recompose";
import { withApollo } from "react-apollo";
import { __ } from "react-pe-utilities";

class Fail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      start: false
    };
    this.timeout = -1;
  }

  componentDidMount() {
    this.timeout = setInterval(() => {
      this.setState({
        start: true
      });
    }, 40);
  }

  componentWillUnmount() {
    clearInterval(this.timeout);
  }

  render() {
    const className = this.state.start ? " animated animation-swipe-right " : " hidden";
    return /*#__PURE__*/React.createElement("div", {
      className: `test-cover ${className}`
    }, /*#__PURE__*/React.createElement("div", {
      className: "lead"
    }, __("FAIL TEST. TIMEOUT")));
  }

}

export default compose(withApollo)(Fail);