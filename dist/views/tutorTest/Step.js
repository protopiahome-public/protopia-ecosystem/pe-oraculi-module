function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import QuestionMultichose from "./QuestionMultichose";
import QuestionTrueFalse from "./QuestionTrueFalse";

class Step extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onStep", () => {
      console.log(this.state.checked);
      this.props.step(`{ id : "${this.props.id}", answers : [ ${this.state.checked} ] }`, this.props.id);
    });

    _defineProperty(this, "onChange", data => {
      if (this.props.type == "multichoice" && !this.props.single) {
        let checked = Array.isArray(this.state.checked) ? [...this.state.checked] : [];
        const r = checked.filter(e => e === data); // console.log(r);

        if (r.length > 0) {
          checked = checked.filter(e => e !== data);
        } else {
          checked.push(data);
        }

        console.log(checked);
        this.setState({
          checked: checked.map(e => e.toString()).join(",")
        });
      } else {
        this.setState({
          checked: data.toString()
        });
      }
    });

    this.state = {
      checked: null,
      step: this.props.step,
      start: false,
      rights: this.props.rights || []
    };
    this.timeout = -1;
  }

  componentDidMount() {
    this.timeout = setInterval(() => {
      this.setState({
        start: true
      });
    }, 100);
  }

  componentWillUnmount() {
    clearInterval(this.timeout);
  }

  componentWillUpdate(nextProps) {
    // console.log( nextProps.rights );
    if (nextProps.i !== this.state.i) {
      this.setState({
        i: nextProps.i,
        start: false
      });
    }

    if (nextProps.checked !== this.state.checked) {
      this.setState({
        checked: nextProps.checked
      });
    }

    if (nextProps.rights && nextProps.rights !== this.state.rights) {
      // console.log( nextProps.rights )
      this.setState({
        rights: nextProps.rights
      });
    }
  }

  render() {
    switch (this.props.type) {
      case "multichoice":
        return /*#__PURE__*/React.createElement(QuestionMultichose, _extends({}, this.props, this.state, {
          onChange: this.onChange,
          onStep: this.onStep
        }));

      case "truefalse":
        return /*#__PURE__*/React.createElement(QuestionTrueFalse, _extends({}, this.props, this.state, {
          onChange: this.onChange,
          onStep: this.onStep
        }));

      case "matching":
      default:
        return /*#__PURE__*/React.createElement(QuestionMultichose, _extends({}, this.props, this.state, {
          onChange: this.onChange,
          onStep: this.onStep
        }));
    }
  }

}

export default Step;