function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import Moment from "react-moment";
import { __ } from "react-pe-utilities";
import TestResults from "./TestResults";

class Finish extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onRestart", () => {
      this.props.onRestart();
    });

    this.state = {
      start: true,
      full_results: this.props.full_results
    };
  }

  componentWillUpdate(nextProps) {
    if (nextProps.full_results != this.state.full_results) {
      console.log(nextProps);
      this.setState({
        full_results: this.state.full_results
      });
    }
  }

  render() {
    console.log(this.props.full_results);
    console.log(new Date(this.props.full_results.start_time * 1000));
    console.log(new Date(this.props.full_results.end_time * 1000));
    const try_count = this.props.try_count > 1 ? /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", null, __("Available ") + this.props.try_count + __(" attempts")), /*#__PURE__*/React.createElement("div", {
      className: "btn btn-primary btn-sm mt-4",
      onClick: this.onRestart
    }, __("Try again"))) : __("You not try this test again");
    const className = this.state.start ? " animated animation-swipe-right " : " hidden";
    return /*#__PURE__*/React.createElement("div", {
      className: `test-cover ${className}`
    }, /*#__PURE__*/React.createElement("div", {
      className: "test-question-title"
    }, /*#__PURE__*/React.createElement("span", {
      className: "thin small mr-2"
    }, __("Finished: ")), /*#__PURE__*/React.createElement("span", {
      dangerouslySetInnerHTML: {
        __html: this.props.post_title
      }
    })), /*#__PURE__*/React.createElement("div", {
      className: "test-result-data row w-100"
    }, /*#__PURE__*/React.createElement("div", {
      className: "test-descr-title col-md-6"
    }, __("Start Time"), ":"), /*#__PURE__*/React.createElement("div", {
      className: "test-descr-cont col-md-6"
    }, /*#__PURE__*/React.createElement(Moment, {
      locale: "ru",
      format: "D.MM.YYYY h:mm:ss a"
    }, new Date(this.props.full_results.start_time * 1000)))), /*#__PURE__*/React.createElement("div", {
      className: "test-result-data row w-100"
    }, /*#__PURE__*/React.createElement("div", {
      className: "test-descr-title col-md-6"
    }, __("End Time"), ":"), /*#__PURE__*/React.createElement("div", {
      className: "test-descr-cont col-md-6"
    }, /*#__PURE__*/React.createElement(Moment, {
      locale: "ru",
      format: "D.MM.YYYY h:mm:ss a"
    }, new Date(this.props.full_results.end_time * 1000)))), /*#__PURE__*/React.createElement("div", {
      className: "test-question-answers-results"
    }, /*#__PURE__*/React.createElement(TestResults, {
      results: this.state.full_results,
      test_id: this.props.id,
      questions: this.props.questions
    })), /*#__PURE__*/React.createElement("div", {
      className: "test-question-answers-cont"
    }, try_count));
  }

}

export default Finish;