function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { Query } from "react-apollo";
import BasicState from "react-pe-basic-view";
import { __ } from "react-pe-utilities";
import { Loading } from 'react-pe-useful';
import { getQueryArgs, getQueryName, queryCollection } from "react-pe-layouts";
import ThemeCard from "./tutor/ThemeCard";

class TutorThemes extends BasicState {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "getRoute", () => "themes");
  }

  addRender() {
    const query_name = getQueryName("Bio_Biology_Theme");
    const query_args = getQueryArgs("Bio_Biology_Theme");
    const query = queryCollection("Bio_Biology_Theme", query_name, query_args);
    return /*#__PURE__*/React.createElement("div", {
      className: "course-card-list"
    }, /*#__PURE__*/React.createElement(Query, {
      query: query
    }, ({
      loading,
      error,
      data,
      client
    }) => {
      if (loading) {
        return /*#__PURE__*/React.createElement(Loading, null);
      }

      if (data) {
        const current_course_id = this.props.user ? this.props.user.current_course ? this.props.user.current_course.id : -1 : -1;
        const current_course_title = this.props.user ? this.props.user.current_course ? this.props.user.current_course.post_title : "" : "";
        const courses = data[query_name].filter(e => e.parent == 0 || !e.parent);
        console.log(courses);
        const coursesList = courses.map((e, i) => /*#__PURE__*/React.createElement(ThemeCard, _extends({
          user: this.props.user
        }, e, {
          key: i,
          i: i
        })));
        return /*#__PURE__*/React.createElement("div", {
          className: "themes-card-list"
        }, coursesList);
      }
    }));
  }

}

export default TutorThemes;