function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { withRouter } from "react-router";
import { compose } from "recompose";
import { Query, withApollo } from "react-apollo";
import gql from "graphql-tag";
import CourseArticleList from "./tutor/CourseArticleList";
import CourseTestList from "./tutor/CourseTestList";
import CourseEventList from "./tutor/CourseEventList";
import ClassLessons from "./tutor/ClassLessons";
import { Loading } from 'react-pe-useful';
import { __ } from "react-pe-utilities";
import BasicState from "react-pe-basic-view";

class TutorThemeLessons extends BasicState {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "getRoute", () => "theme-lessons");

    _defineProperty(this, "onTab", evt => {
      const tab = evt.currentTarget.getAttribute("tab");
      this.setState({
        tab
      });
    });

    _defineProperty(this, "onLeave", () => {
      this.setState({
        tab: this.state.tabs[0].name
      });
    });
  }

  basic_state_data() {
    return {
      tabs: [{
        name: "articles",
        title: "Articles",
        component: CourseArticleList
      }, {
        name: "tests",
        title: "Tests",
        component: CourseTestList
      }, {
        name: "events",
        title: "Events",
        component: CourseEventList
      }]
    };
  }

  render() {
    const {
      id
    } = this.props.match.params;
    const type = "Bio_Biology_Theme";
    const query_name = `get${type}`;
    const query = gql`query ${query_name}($id:String)
		{
			${query_name}(id:$id)
			{
				id
				post_title
				post_content
				articles
				{
					id
					post_title
					post_content
					is_free
					is_logged_in
					thumbnail
					is_favorite
				}
				bio_test
				{
					id
				}
				bio_event
				{
					id
				}
				parent
				{
					id
					post_title
				}
				children {
					id
					post_title
					post_content
					children 
					{
						id
						post_title
						post_content
						children 
						{
							id
							post_title
							post_content
							children 
							{
								id
								post_title
								post_content
								children 
								{
									id
									post_title
									post_content
									children 
									{
										id
										post_title
										post_content
									}
								}
							}
						}
					}
					__typename
				}
				
				__typename
			}
		}`;
    return /*#__PURE__*/React.createElement("div", {
      className: "layout-state p-0"
    }, /*#__PURE__*/React.createElement(Query, {
      query: query,
      variables: {
        id
      }
    }, ({
      loading,
      error,
      data,
      client
    }) => {
      if (loading) {
        return /*#__PURE__*/React.createElement(Loading, null);
      }

      if (data) {
        console.log(data[query_name]);
        const course = data[query_name] || {};
        const pic = course.thumbnail != "false" ? course.thumbnail : "";
        return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
          className: "course__header",
          style: {
            backgroundImage: `url(${pic})`
          }
        }, /*#__PURE__*/React.createElement("div", {
          className: "z-index-10 container"
        }, /*#__PURE__*/React.createElement("div", {
          className: "title text-light"
        }, `${__("Theme")}: ${course.post_title}`))), /*#__PURE__*/React.createElement(ClassLessons, {
          tabs: this.state.tabs,
          route: this.getRoute(),
          user: this.props.user,
          course: { ...course
          },
          id: this.props.match.params.id,
          onLeave: this.onLeave,
          type: "bio_biology_theme"
        }));
      }

      if (error) {
        return error.toString();
      }
    }));
  }

}

export default compose(withApollo, withRouter)(TutorThemeLessons);