function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { withRouter } from "react-router";
import { compose } from "recompose";
import { withApollo } from "react-apollo";
import { __ } from "react-pe-utilities";
import BasicState from "react-pe-basic-view";
import TestQuote from "./tutor/TestQuote"; //import Feed from "../../../layouts/BasicState/Feed"

import { Feed } from "react-pe-basic-view";
import { initArea } from "react-pe-utilities";

class TutorTests extends BasicState {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "getRoute", () => "all-tests");
  }

  addRender() {
    const {
      id,
      articles
    } = this.props;
    const data_type = "Bio_Test";
    const paging = "";
    return /*#__PURE__*/React.createElement("div", {
      className: " mt-5"
    }, /*#__PURE__*/React.createElement("div", {
      className: "tutor-row"
    }, /*#__PURE__*/React.createElement("div", {
      className: "tutor-left-aside-2  mobile-relatived"
    }, initArea("tutor-tests-left-aside", { ...this.props
    })), /*#__PURE__*/React.createElement("div", {
      className: "tutor-main-2"
    }, /*#__PURE__*/React.createElement(Feed, {
      component: TestQuote,
      data_type: data_type,
      offset: 0,
      paging: paging
    }), initArea("tutor-tests", { ...this.props
    })), /*#__PURE__*/React.createElement("div", {
      className: "tutor-right-aside-2"
    }, initArea("tutor-tests-right-aside", { ...this.props
    }))));
  }

}

export default compose(withApollo, withRouter)(TutorTests);