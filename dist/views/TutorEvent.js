function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { withRouter } from "react-router";
import { compose } from "recompose";
import { Query, withApollo } from "react-apollo";
import Moment from "react-moment";
import gql from "graphql-tag";
import { __ } from "react-pe-utilities";
import BasicState from "react-pe-basic-view";
import EventParticipation from "./tutor/EventParticipation";
import { Loading } from 'react-pe-useful';
import { initArea } from "react-pe-utilities";

class TutorEvent extends BasicState {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "getRoute", () => "course-lessons");
  }

  render() {
    const {
      id
    } = this.props.match.params;
    const query = gql`query getBio_Event($id:String) {
			getBio_Event(id:$id) { 
				id
				post_title
				post_content
				post_date
				thumbnail
				time
				member_status
				request_form
				post_author
				{
				  id
				  display_name
				  avatar
				}
				bio_course
				{
				  id
				  post_title
				  logotype
				  children
				  {
					  id
					  post_title					  
					  children
					  {
						  id
						  post_title
						  
					  }
				  }
				}
				bio_olimpiad_type
				{
				  id
				  post_title
				}
				bio_class
				{
				  id
				  post_title
				}
				bio_biology_theme
				{
				  id
				  post_title
				  thumbnail
				}
				comments
				{
					id
					discussion_id
					discussion_type
					content
					parent_id
					author
					{
						avatar
						display_name
						id
					}
					date
					is_approved
					
				}
				__typename
			  }
		  }
		  `;
    return /*#__PURE__*/React.createElement("div", {
      className: "layout-state"
    }, /*#__PURE__*/React.createElement(Query, {
      query: query,
      variables: {
        id
      }
    }, ({
      loading,
      error,
      data,
      client
    }) => {
      if (loading) {
        return /*#__PURE__*/React.createElement("div", {
          className: "layout-state bg-white"
        }, /*#__PURE__*/React.createElement(Loading, null));
      }

      if (data) {
        console.log(data.getBio_Event);
        const event = data.getBio_Event || {};
        const classes = event.bio_class.map((e, i) => /*#__PURE__*/React.createElement("div", {
          className: "tutor-label-class",
          key: i
        }, `${e.post_title} ${__("class")}`));
        const themes = event.bio_biology_theme.map((e, i) => /*#__PURE__*/React.createElement("div", {
          className: "tutor-label",
          key: i
        }, e.post_title));
        const courses = event.bio_course.map((e, i) => /*#__PURE__*/React.createElement("div", {
          className: "tutor-label-course",
          key: i
        }, e.post_title));
        const bio_olimpiad_type = event.bio_olimpiad_type.map((e, i) => /*#__PURE__*/React.createElement("div", {
          className: "tutor-label-olympiad_type",
          key: i
        }, e.post_title));
        return /*#__PURE__*/React.createElement("div", {
          className: " mt-5"
        }, /*#__PURE__*/React.createElement("div", {
          className: "tutor-row"
        }, /*#__PURE__*/React.createElement("div", {
          className: "tutor-left-aside  mobile-relatived"
        }, initArea("single-event-left-aside", { ...event,
          user: this.props.user,
          prefix: "events"
        })), /*#__PURE__*/React.createElement("div", {
          className: "tutor-main"
        }, /*#__PURE__*/React.createElement("div", {
          className: "tutor-article-title"
        }, event.post_title), /*#__PURE__*/React.createElement("div", {
          className: "lead mb-3"
        }, /*#__PURE__*/React.createElement(Moment, {
          locale: "ru",
          format: "D MMMM YYYY"
        }, new Date(event.time * 1000))), /*#__PURE__*/React.createElement("div", {
          className: "event-classes"
        }, classes), /*#__PURE__*/React.createElement("div", {
          className: "event-themes"
        }, themes), /*#__PURE__*/React.createElement("div", {
          className: "event-courses"
        }, courses), /*#__PURE__*/React.createElement("div", {
          className: "event-courses"
        }, bio_olimpiad_type), /*#__PURE__*/React.createElement("div", {
          className: "mt-5",
          dangerouslySetInnerHTML: {
            __html: event.post_content
          }
        }), /*#__PURE__*/React.createElement("div", {
          className: " pt-3 "
        }, /*#__PURE__*/React.createElement(EventParticipation, _extends({
          user: this.props.user
        }, event))), initArea("single-event", { ...event,
          user: this.props.user,
          prefix: "events"
        })), /*#__PURE__*/React.createElement("div", {
          className: "tutor-right-aside"
        }, initArea("single-event-right-aside", { ...event,
          user: this.props.user,
          prefix: "events"
        }))));
      }

      if (error) {
        return error.toString();
      }
    }));
  }

}

export default compose(withApollo, withRouter)(TutorEvent);