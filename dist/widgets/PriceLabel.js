function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Intent } from "@blueprintjs/core";
import gql from "graphql-tag";
import { sprintf } from "react-pe-utilities";
import { AppToaster } from 'react-pe-useful';
import { __ } from "react-pe-utilities";
import { withApollo } from "react-apollo";
const query = gql`query getRobokassaRedirectUrl($course_id: Int) {
  getRobokassaRedirectUrl(course_id: $course_id)
}`;

class PriceLabel extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "getPrice", () => {
      if (typeof this.props.price == "object") {
        for (const p in this.props.price) {
          return this.props.price[p];
        }
      } else {
        return this.props.price;
      }
    });

    _defineProperty(this, "getPriceChanger", () => {
      if (typeof this.props.price == "object") {
        const options = [];

        for (const p in this.props.price) {
          options.push( /*#__PURE__*/React.createElement("option", {
            value: this.props.price[p],
            key: p
          }, p));
        }

        return /*#__PURE__*/React.createElement("select", {
          className: "price-selector"
        }, options);
      }

      return null;
    });

    _defineProperty(this, "onPay", evt => {
      this.props.client.query({
        query,
        variables: {
          course_id: this.props.id
        }
      }).then(result => {
        window.location.href = result.data.getRobokassaRedirectUrl;
      }, error => {
        AppToaster.show({
          intent: Intent.DANGER,
          icon: "error",
          message: __("Unkorrect message")
        });
      });
    });

    this.state = {
      currentPrice: 0,
      selected: -1
    };
  }

  render() {
    const price = this.getPrice();
    const text = price ? sprintf(__("Купить за %s руб."), parseInt(price)) : __("Бесплатно");

    const pay_descr = __(price ? "Купить" : "Пройти");

    return this.props.is_closed ? null : /*#__PURE__*/React.createElement("div", {
      className: "d-flex py-5"
    }, /*#__PURE__*/React.createElement("div", {
      className: "price-data"
    }, text, /*#__PURE__*/React.createElement("div", {
      className: "btn btn-danger btn-lg ml-4",
      onClick: this.onPay
    }, pay_descr)));
  }

}

export default withApollo(PriceLabel);