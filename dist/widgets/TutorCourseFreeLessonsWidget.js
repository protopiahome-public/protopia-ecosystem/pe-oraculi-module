import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { __ } from "react-pe-utilities";

class TutorCourseFreeLessonsWidget extends Component {
  render() {
    const lessons = this.props.articles ? this.props.articles.filter(e => e.is_free).map((e, i) => /*#__PURE__*/React.createElement(NavLink, {
      key: i,
      to: `/article/${e.id}`,
      className: " "
    }, /*#__PURE__*/React.createElement("div", {
      className: "lesson-thumbnail",
      style: {
        backgroundImage: `url(${e.thumbnail})`
      }
    }, /*#__PURE__*/React.createElement("div", {
      className: ""
    }, e.post_title)))) : null;
    console.log(lessons);
    return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
      className: "aside-widget-title"
    }, __("Free articles:")), lessons.length > 0 ? /*#__PURE__*/React.createElement("div", null, lessons) : /*#__PURE__*/React.createElement("div", {
      className: "alert alert-secondary"
    }, __("No articles exists")));
  }

}

export default TutorCourseFreeLessonsWidget;