function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { compose } from "recompose";
import { withApollo } from "react-apollo";
import { Icon, PopoverInteractionKind, Intent, Dialog, Button, ButtonGroup, Popover, Position } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import Answer from "./Answer";

class AnswerMatching extends Answer {
  constructor(props) {
    super(props);

    _defineProperty(this, "onSubAnswer", evt => {
      this.setState({
        post_content: evt.currentTarget.value
      });
    });

    this.state = { ...this.props,
      value: "",
      dialogTitle: __("Dialog title"),
      dialogContent: __("Dialog Content"),
      search: [],
      saved: typeof props.saved == "undefined" ? true : props.saved,
      result: {
        post_title: props.post_title,
        id: props.id
      }
    };
  }

  render() {
    const saved = this.state.saved ? /*#__PURE__*/React.createElement(Icon, {
      icon: "tick",
      intent: Intent.SUCCESS
    }) : /*#__PURE__*/React.createElement(Icon, {
      icon: "error",
      intent: Intent.DANGER
    });
    return /*#__PURE__*/React.createElement("div", {
      className: `row data-list question ${this.state.selected ? "active" : ""}`,
      id: this.state.id,
      onMouseDown: this.onSelect
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-1 py-2"
    }, /*#__PURE__*/React.createElement(ButtonGroup, {
      vertical: true
    }, /*#__PURE__*/React.createElement(Button, {
      icon: "caret-up",
      minimal: true,
      onClick: () => this.props.onSwitchUp(this.state.id),
      disabled: this.props.is_first
    }), /*#__PURE__*/React.createElement("input", {
      type: "number",
      className: "input dark",
      value: this.state.i,
      style: {
        width: 50,
        textAlign: "center"
      },
      onChange: this.onSwitchOrder
    }), /*#__PURE__*/React.createElement(Button, {
      icon: "caret-down",
      minimal: true,
      onClick: () => this.props.onSwitchDn(this.props.id),
      disabled: this.props.is_last
    }))), /*#__PURE__*/React.createElement("div", {
      className: "col-5 py-2"
    }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("textarea", {
      value: this.state.post_content,
      onChange: this.onTextarea,
      className: "w-100 p-4 bg-transparent border-0",
      rows: "6"
    }))), /*#__PURE__*/React.createElement("div", {
      className: "col-5 py-2"
    }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("textarea", {
      value: this.state.subAnswer ? this.state.subAnswer.post_content : "",
      onChange: this.onSubAnswer,
      className: "w-100 p-4 bg-transparent border-0",
      rows: "6"
    }))), /*#__PURE__*/React.createElement("div", {
      className: "col-1 py-2"
    }, /*#__PURE__*/React.createElement("div", {
      className: "mb-4"
    }, saved), /*#__PURE__*/React.createElement(Popover, {
      interactionKind: PopoverInteractionKind.CLICK,
      popoverClassName: "bp3-popover-content-sizing",
      position: Position.LEFT,
      content: /*#__PURE__*/React.createElement(ButtonGroup, {
        vertical: true
      }, /*#__PURE__*/React.createElement(Button, {
        minimal: true,
        onClick: this.onAddQuestion
      }, __("Add Question after")), /*#__PURE__*/React.createElement(Button, {
        minimal: true,
        onClick: this.onDlQuestion
      }, __("Remove Question")))
    }, /*#__PURE__*/React.createElement(Button, {
      minimal: true
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-ellipsis-v"
    })))), /*#__PURE__*/React.createElement(Dialog, {
      isOpen: this.state.isChQ,
      onClose: this.onChQ,
      title: this.state.dialogTitle,
      className: "little"
    }, this.state.dialogContent), /*#__PURE__*/React.createElement(Dialog, {
      isOpen: this.state.isChQOpen,
      onClose: this.onOpenQuestion,
      title: __("Change Question"),
      className: "little"
    }, "fgg"));
  }

}

export default compose(withApollo)(AnswerMatching);