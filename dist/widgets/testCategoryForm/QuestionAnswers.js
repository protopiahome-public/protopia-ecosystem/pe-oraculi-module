function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";
import { compose } from "recompose";
import { withApollo } from "react-apollo";
import gql from "graphql-tag";
import $ from "jquery";
import { Button } from "@blueprintjs/core";
import Answer from "./Answer";
import AnswerMatching from "./AnswerMatching";
import { Loading } from 'react-pe-useful';
import { __ } from "react-pe-utilities";

class QuestionAnswers extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onSwitchUp", data => {
      const answers = [...this.state.answers];
      this.state.answers.forEach((e, i) => {
        if (e.id === data) {
          const me = answers.splice(i, 1);
          answers.splice(i - 1, 0, me[0]);
        }
      });
      $(".question").removeClass("active");
      this.setState({
        answers,
        selected: data
      }, () => $(`#${data}`).addClass("active"));
      if (this.props.onChange) this.props.onChange(["answers", answers.filter(e => !e.isNew).map(e => {
        delete e.saved;
        return e;
      })]);
    });

    _defineProperty(this, "onSwitchDn", data => {
      const answers = [...this.state.answers];
      this.state.answers.forEach((e, i) => {
        if (e.id === data) {
          const me = answers.splice(i, 1);
          answers.splice(i + 1, 0, me[0]);
        }
      });
      $(".question").removeClass("active");
      this.setState({
        answers,
        selected: data
      }, () => $(`#${data}`).addClass("active"));
      if (this.props.onChange) this.props.onChange(["answers", answers.filter(e => !e.isNew).map(e => {
        delete e.saved;
        return e;
      })]);
    });

    _defineProperty(this, "onInsertQuestion", () => {
      const answers = [...this.state.answers];
      const newId = this.state.answers.length.toString();
      answers.push({
        post_title: "",
        id: newId,
        saved: false,
        isNew: true
      });
      $(".question").removeClass("active");
      this.setState({
        answers,
        selected: newId
      });
      if (this.props.onChange) this.props.onChange(["answers", answers.filter(e => !e.isNew).map(e => {
        delete e.saved;
        return e;
      })]);
    });

    _defineProperty(this, "onAddQuestion", data => {
      const answers = [...this.state.answers];
      const newId = this.state.answers.length.toString();
      this.state.answers.forEach((e, i) => {
        if (e.id === data) {
          const me = {
            post_title: "",
            id: newId,
            saved: false,
            isNew: true
          };
          answers.splice(i + 1, 0, me);
        }
      });
      $(".question").removeClass("active");
      this.setState({
        answers,
        selected: newId
      });
      if (this.props.onChange) this.props.onChange(["answers", answers.filter(e => !e.isNew).map(e => {
        delete e.saved;
        return e;
      })]);
    });

    _defineProperty(this, "onSwitchOrder", data => {
      const answers = [...this.state.answers];
      this.state.answers.forEach((e, i) => {
        if (e.id === data[0]) {
          const me = answers.splice(i, 1);
          answers.splice(data[1], 0, me[0]);
        }
      });
      $(".question").removeClass("active");
      this.setState({
        answers,
        selected: data[0]
      }, () => $(`#${data[0]}`).addClass("active"));
      if (this.props.onChange) this.props.onChange(["answers", answers.filter(e => !e.isNew).map(e => {
        delete e.saved;
        return e;
      })]);
    });

    _defineProperty(this, "changeQuestion", data => {
      const answers = [...this.state.answers];
      let me;
      this.state.answers.forEach((e, i) => {
        if (e.id === data[0]) {
          me = answers.splice(i, 1, data[1]);
        }
      }); // console.log("added:", 	[ ...this.state.added, data[1] ] );

      const deleted = me[0].isNew ? this.state.deleted : [...this.state.deleted, me[0]];
      const added = [...this.state.added, data[1]];
      $(".question").removeClass("active");
      this.setState({
        answers,
        selected: data[1].id,
        deleted,
        added
      }, () => $(`#${data[1].id}`).addClass("active"));
      if (this.props.onChange) this.props.onChange(["answers", answers.filter(e => !e.isNew).map(e => {
        delete e.saved;
        return e;
      })]);
    });

    _defineProperty(this, "onDelete", data => {
      const answers = [...this.state.answers];
      let me;
      this.state.answers.forEach((e, i) => {
        if (e.id === data) {
          me = answers.splice(i, 1);
        }
      });
      $(".question").removeClass("active");
      this.setState({
        answers,
        selected: -1,
        deleted: me[0].isNew ? this.state.deleted : [...this.state.deleted, me[0]]
      });
      if (this.props.onChange) this.props.onChange(["answers", answers.filter(e => !e.isNew).map(e => {
        delete e.saved;
        return e;
      })]);
    });

    this.state = { ...props,
      answers: [],
      selected: null,
      loading: true,
      added: [],
      deleted: []
    };
  }

  componentDidMount() {
    const test_id = `"${this.props.id}"`;
    const query = gql`
			query
			{
			  getBio_TestQuestion ( id: ${test_id} )
			  {
				id
				type
				answers 
				{
					id
					is_deleted
					is_subquestion
					subquestion_answer_id
					order
					post_content
					__typename
				}
			  }
			}
		`;
    this.props.client.query({
      query,
      variables: {
        id: test_id
      }
    }).then(result => {
      console.log(result.data.getBio_TestQuestion.answers);
      this.setState({
        answers: result.data.getBio_TestQuestion.answers,
        loading: false
      });
    });
  }

  render() {
    if (this.state.loading) return /*#__PURE__*/React.createElement(Loading, null);
    let answers;

    if (this.state.answers) {
      switch (this.state.type) {
        case "matching":
          answers = this.state.answers.filter(e => !e.is_subquestion).map((e, i) => {
            const subAnswer = this.state.answers.filter((ee, i) => ee.subquestion_answer_id === e.id)[0];
            return /*#__PURE__*/React.createElement(AnswerMatching, _extends({
              key: i
            }, e, {
              subAnswer: subAnswer,
              i: i,
              is_first: i === 0,
              is_last: i === this.state.answers.length - 1,
              selected: this.state.selected === e.id,
              onSwitchUp: this.onSwitchUp,
              onSwitchDn: this.onSwitchDn,
              onSwitchOrder: this.onSwitchOrder,
              onAddQuestion: this.onAddQuestion,
              changeQuestion: this.changeQuestion,
              onDelete: this.onDelete
            }));
          });
          break;

        case "truefalse":
          answers = /*#__PURE__*/React.createElement("div", {
            className: "alert alert-secondary"
          }, __("No edit answers true and false."));
          break;

        case "multichoice":
        default:
          answers = this.state.answers.map((e, i) => /*#__PURE__*/React.createElement(Answer, _extends({
            key: i
          }, e, {
            i: i,
            single: this.state.single,
            name: this.props.id,
            is_first: i === 0,
            is_last: i === this.state.answers.length - 1,
            selected: this.state.selected === e.id,
            onSwitchUp: this.onSwitchUp,
            onSwitchDn: this.onSwitchDn,
            onSwitchOrder: this.onSwitchOrder,
            onAddQuestion: this.onAddQuestion,
            changeQuestion: this.changeQuestion,
            onDelete: this.onDelete
          })));
          break;
      }
    }

    return /*#__PURE__*/React.createElement("div", {
      className: "row"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-md-3"
    }), /*#__PURE__*/React.createElement("div", {
      className: "col-md-9"
    }, /*#__PURE__*/React.createElement(Link, {
      to: this.props.parent_route,
      className: "btn btn-link"
    }, __("edit Question"))), /*#__PURE__*/React.createElement("div", {
      className: "col-md-3"
    }), /*#__PURE__*/React.createElement("div", {
      className: "col-md-9 "
    }, answers), /*#__PURE__*/React.createElement("div", {
      className: "col-md-3"
    }), /*#__PURE__*/React.createElement("div", {
      className: "col-md-9 "
    }, /*#__PURE__*/React.createElement(Button, {
      className: "ml-2 my-2",
      onClick: this.onInsertQuestion,
      icon: "plus"
    })));
  }

}

export default compose(withApollo, withRouter)(QuestionAnswers);