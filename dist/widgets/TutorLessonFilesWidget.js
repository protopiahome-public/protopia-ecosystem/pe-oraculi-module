import React, { Component } from "react";
import { __ } from "react-pe-utilities";

class TutorLessonFilesWidget extends Component {
  render() {
    const dnld = [1, 2, 3, 4].map((e, i) => {
      if (this.props[`include_id${e}`] === "false") {
        return null;
      }

      return /*#__PURE__*/React.createElement("a", {
        className: "btn btn-secondary btn-lg m-1",
        href: this.props[`include_id${e}`],
        download: true,
        key: i
      }, /*#__PURE__*/React.createElement("i", {
        className: "far fa-file-archive"
      }));
    });
    return /*#__PURE__*/React.createElement("div", {
      className: "course_category_select"
    }, /*#__PURE__*/React.createElement("div", {
      className: "aside-widget-title"
    }, __("Matherials for Lesson:")), dnld);
  }

}

export default TutorLessonFilesWidget;