import React from "react";
import gql from "graphql-tag";
import { __ } from "react-pe-utilities";

class EventMembersWidget extends React.Component {
  constructor(props) {
    super(props);
    this.state = { ...props,
      users: [],
      selected: null,
      loading: true,
      added: [],
      deleted: []
    };
  }

  refresh() {
    const test_id = `"${this.props.id}"`;
    const query = gql`
			query getEventMembers
			{
					getEventMembers(paging:{
					event:[${test_id}]
				})
				{
					user
					{
						id
						display_name
						user_email
					}
				}
			}
		`;
    this.props.client.query({
      query,
      variables: {
        id: test_id
      }
    }).then(result => {
      console.log(result.data.getEventMembers[0].user);
      this.setState({
        users: result.data.getEventMembers[0].user,
        loading: false
      });
    });
  }

  componentDidMount() {
    this.refresh();
  }

  render() {
    console.log(this.props);

    const _users = this.state.users.length > 0 ? this.state.users.map((e, i) => /*#__PURE__*/React.createElement("div", {
      className: "row mx-1"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-md-4 pb-2 border-bottom border-secondary"
    }, e.display_name), /*#__PURE__*/React.createElement("div", {
      className: "col-md-8 pb-2 border-bottom border-secondary"
    }, e.user_email))) : /*#__PURE__*/React.createElement("div", {
      className: "alert alert-secondary"
    }, __("No members exists"));

    return this.props.data_type == "Bio_Event" ? /*#__PURE__*/React.createElement("div", {
      className: "row dat my-4"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-md-3 layout-label"
    }, __("Event members")), /*#__PURE__*/React.createElement("div", {
      className: "col-md-9 "
    }, /*#__PURE__*/React.createElement("div", {
      className: ""
    }, _users))) : null;
  }

}

export default EventMembersWidget;