import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { __ } from "react-pe-utilities";

class TutorCurrentFacultetWidget extends Component {
  render() {
    if (!this.props.bio_facultet || !Array.isArray(this.props.bio_facultet) || this.props.bio_facultet.length == 0) return null;
    const courses = this.props.bio_facultet.map((e, i) => {
      const cl = "tutor-course-widget-cont";
      return /*#__PURE__*/React.createElement("div", {
        className: "tutor-course-widget-container",
        key: i
      }, /*#__PURE__*/React.createElement(NavLink, {
        className: cl,
        to: `/facultet/${e.id}`
      }, /*#__PURE__*/React.createElement("div", {
        className: "tutor-course-widget-logo",
        style: {
          backgroundImage: `url(${e.thumbnail})`
        }
      }), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
        className: "tutor-course-widget-title"
      }, e.post_title))));
    });
    return /*#__PURE__*/React.createElement("div", {
      className: "course_category_select mb-5"
    }, /*#__PURE__*/React.createElement("div", {
      className: "aside-widget-title"
    }, __("Facultets")), /*#__PURE__*/React.createElement("div", null, courses));
  }

}

export default TutorCurrentFacultetWidget;